// Movie.java
// Creator: Carson Lance

package mypackage;

public class Movie {
  String title;
  int release_year;
  String genre;

  public Movie(String title, int release_year, String genre) {
    this.title = title;
    this.release_year = release_year;
    this.genre = genre;
  }

  public String getTitle() {
    return this.title;
  }

  public int getRelease() {
    return this.release_year;
  }

  public String getGenre() {
    return this.genre;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setRelease(int release_year) {
    this.release_year = release_year;
  }

  public void setGenre(String genre) {
    this.genre = genre;
  }

  public void print() {
    System.out.println("Title: " + this.title + "\tRelease year: " + this.release_year + "\tGenre: " + this.genre);
  }
}
