// Daily1.java
// Creator: Carson Lance

import mypackage.Movie;
import java.util.ArrayList;
import java.util.Scanner;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.*;

public class Daily1 {

  public ArrayList<String> title_year_split(String full) {
    ArrayList<String> title_year = new ArrayList<String>();
    title_year = full.split();
    return title_year;
  }

  public Movie json_to_movie(JSONObject jo) {
    String t = (String) jo.get("title");

    String myearString = (String) jo.get("releaseYear");
    int myear = Integer.parseInt(myearString);

    String allGenre = "";
    JSONArray genreArray  = (JSONArray) jo.get("genre");
    for (Object oGenre : genreArray) {
      String oneGenre = (String) oGenre;
      allGenre = allGenre + ", " + oneGenre;
    }

    Movie m = new Movie(title, myear, allgenre);

    return m;
  }

  public void printall(ArrayList<Movie> movie_arr) {
    for (Movie m: movie_arr) {
      m.print();
    }
  }
  public static void main(String[] args) {
    ArrayList<Movie> movie_arr = new ArrayList<Movie>();
    Object obj = new JSONParser().parse(new FileReader("movies.json"));

    JSONArray ja = (JSONObject) obj;

    for (Object o: ja) {
      JSONObject jo = (JSONObject) o;
      movie_arr.add(json_to_movie(jo));
    }

    Scanner sc = new Scanner(System.in);
    String input = "";
    int index;

    while(!input.toLowerCase().equals("exit")){
      System.out.println("Available Actions:\n\tprintall\tPrints all movies in plain text.\n\tprint <integer>\tPrints the movie at the JSON Array index specified by the integer object.");
      System.out.println("Enter action: ");
      input = sc.nextLine();
      System.out.println(input);
      if (input.equals("exit")) {
        break;
      }
      else {
        if (input.equals("printall")) {
          printall(movie_arr);
        }
        else {
          input = sc.nextLine();
          index = Integer.parseInt(input);
        }
      }
    }
  }
}
